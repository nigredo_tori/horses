﻿using UnityEngine;
using System.Collections;

namespace Horse
{
    /// <summary>
    /// Константы с идентификаторами слоёв.
    /// </summary>
    public static class Layers
    {
        public static readonly int PLAYER = LayerMask.NameToLayer("Player");
        public static readonly int COLLIDES_WITH_PLAYER = LayerMask.NameToLayer("COLLIDES_WITH_PLAYER");
    }
}