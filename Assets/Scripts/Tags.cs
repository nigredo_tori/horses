﻿using UnityEngine;
using System.Collections;

namespace Horse
{
    /// <summary>
    /// Константы с именами тегов
    /// </summary>
    public static class Tags
    {

        public static readonly string SPAWN_POINT = "Spawn Point";
    }
}