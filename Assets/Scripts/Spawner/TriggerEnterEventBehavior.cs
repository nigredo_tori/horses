﻿using UnityEngine;
using System.Collections;
using System;

namespace Horse.Spawn
{
    /// <summary>
    /// Оповещает подписчиков при получении OnTriggerEnter(Collider)
    /// </summary>
    public class TriggerEnterEventBehavior : MonoBehaviour
    {
        public event Action<TriggerEnterEventBehavior, Collider> TriggerEntered;

        public void OnTriggerEnter(Collider other)
        {
            if (TriggerEntered != null)
            {
                TriggerEntered(this, other);
            }
        }
    }
}