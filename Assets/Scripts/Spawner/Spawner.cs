﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Horse.Spawn
{

    /// <summary>
    /// Управляет созданием и уничтожением объектов в зависимости от расстояния до игрока.
    /// 
    /// Использует объекты с тегом "Spawn Point" как точки, в которых появляются объекты.
    /// Когда игрок подходит к такой точке на расстояние ближе заданного - с заданной вероятностью создаётся новый объект.
    /// Объекты, находящиеся дальше от игрока чем заданное расстояние, возвращаются в пул.
    /// 
    /// Предполагает что точки появления находятся на слое, допускающем столкновения с объектом игрока (и только с ним).
    /// </summary>
    public class Spawner : MonoBehaviour
    {

        [Tooltip("Вероятность появления объекта в точке (при условии наличия места в пуле)")]
        [Range(0, 1)]
        public float spawnProbability = 1;

        [Tooltip("Расстояние от игрока, на котором создаются новые объекты")]
        public float spawnRadius = 50;

        [Tooltip("Расстояние от игрока, за пределами которого префа уничтожаются")]
        public float despawnRadius = 75;

        [Tooltip("Максимальное количество одновременно активных объектов")]
        public int maxObjectCount = 10;

        [Tooltip("Префаб для создания объектов")]
        public GameObject objectPrefab;

        [Tooltip("Положение игрока, используемое для проверки расстояния")]
        public Transform playerTransform;

        // Пул созданных объектов (активных и неактивных). Активные объекты находятся в пуле после неактивных.
        private IList<GameObject> objects;

        // Индекс первого активного объекта в objects, либо objects.Count если активных объектов нет.
        private int firstActiveObjectIndex;

        public void Start()
        {
            objects = new List<GameObject>();

            GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag(Tags.SPAWN_POINT);

            foreach (var spawnPoint in spawnPoints)
            {
                InitSpawnPoint(spawnPoint);
            }
        }

        public void FixedUpdate()
        {
            // Удаляем все объекты, находящиеся дальше чем despawnRadius от игрока

            for (int i = firstActiveObjectIndex; i < objects.Count; i++)
            {
                if (ShouldDespawnObject(objects[i]))
                {
                    DespawnObjectAt(i);
                }
            }
        }

        private void DespawnObjectAt(int index)
        {
            GameObject obj = objects[index];
            obj.SetActive(false);

            ReleaseObjectAt(index);
        }

        /// <summary>
        /// Возврат объекта в пул.
        /// </summary>
        /// <param name="index"></param>
        private void ReleaseObjectAt(int index)
        {
            // меняем местами скрываемый объект и первый активный
            var t = objects[index];
            objects[index] = objects[firstActiveObjectIndex];
            objects[firstActiveObjectIndex] = t;

            ++firstActiveObjectIndex;
        }

        private bool ShouldDespawnObject(GameObject obj)
        {
            float sqrDistance = (obj.transform.position - playerTransform.position).sqrMagnitude;
            return sqrDistance > despawnRadius * despawnRadius;
        }

        private void InitSpawnPoint(GameObject spawnPoint)
        {
            var collider = spawnPoint.AddComponent<SphereCollider>();
            collider.radius = spawnRadius;
            collider.isTrigger = true;

            var eventBehavior = spawnPoint.AddComponent<TriggerEnterEventBehavior>();
            eventBehavior.TriggerEntered += TriggerEnterEventBehavior_TriggerEntered;

        }

        private void TriggerEnterEventBehavior_TriggerEntered(TriggerEnterEventBehavior spawnPoint, Collider player)
        {
            if (ShouldSpawnObject())
            {
                SpawnObject(spawnPoint.transform.position);
            }
        }

        private bool ShouldSpawnObject()
        {
            return firstActiveObjectIndex < maxObjectCount
                && UnityEngine.Random.value < spawnProbability;
        }

        private void SpawnObject(Vector3 position)
        {
            GameObject obj = AquireObject();
            obj.transform.position = position;
            obj.SetActive(true);
        }

        /// <summary>
        /// Получение объекта из пула. Если в пуле недостаточно объектов - создаётся новый.
        /// </summary>
        /// <returns></returns>
        private GameObject AquireObject()
        {
            if (firstActiveObjectIndex > 0)
            {
                // В пуле уже есть неактивный объект
                --firstActiveObjectIndex;
                return objects[firstActiveObjectIndex];
            }
            else
            {
                var result = CreateObject();
                objects.Add(result);
                return result;
            }
        }

        /// <summary>
        /// Создание нового объекта
        /// </summary>
        /// <returns></returns>
        private GameObject CreateObject()
        {
            GameObject result = GameObject.Instantiate(objectPrefab);

            // Чтобы не загромождать иерархию в редакторе
            result.transform.parent = transform;

            return result;
        }
    }
}