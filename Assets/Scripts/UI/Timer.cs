﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

namespace Horse.UI
{
    /// <summary>
    /// Управляет отображением времени с момента загрузки уровня.
    /// </summary>
    [RequireComponent(typeof(Text))]
    public class Timer : MonoBehaviour
    {

        private Text text;

        private float startTime;
        private int fullSeconds;

        void Start()
        {
            text = GetComponent<Text>();
            startTime = Time.fixedTime;
        }

        public void FixedUpdate()
        {
            int newFullSeconds = (int)(Time.fixedTime - startTime);
            if (newFullSeconds != fullSeconds)
            {
                fullSeconds = newFullSeconds;
                text.text = FormatSeconds(fullSeconds);
            }
        }

        private static string FormatSeconds(int fullSeconds)
        {
            TimeSpan span = new TimeSpan(0, 0, fullSeconds);
            return String.Format(@"{0}:{1:00}", (int)span.TotalMinutes, span.Seconds);
        }
    }
}