﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Horse.Actor;

namespace Horse.UI
{
    /// <summary>
    /// Отображает полосу выносливости лошади
    /// </summary>
    [RequireComponent(typeof(Slider))]
    public class EnergyBar : MonoBehaviour
    {

        [Tooltip("Управляемая пользователем лошадь")]
        public HorseUserController controller;

        private HorseStaminaModel staminaModel;

        private Slider slider;
        
        void Start()
        {
            staminaModel = controller.staminaModel;
            slider = GetComponent<Slider>();

            slider.interactable = false;
        }
        
        void Update()
        {
            slider.normalizedValue = staminaModel.Stamina;
        }
    }
}