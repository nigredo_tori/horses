﻿using UnityEngine;
using System.Collections;

namespace Horse.Actor
{
    /// <summary>
    /// Реализует поведение лошади-бота.
    /// </summary>
    public class HorseAIController : HorseControllerBase
    {

        [Tooltip("Радиус, в котором выбирается цель движения.")]
        public float targetSelectionRadius = 50;

        [Tooltip("Минимальное время между сменами цели движения, с.")]
        public float minChangeInterval = 1;

        [Tooltip("Максимальное время между сменами цели движения, с.")]
        public float maxChangeInterval = 3;

        [Tooltip(@"Корневой объект меша, используется для поворота лошади.
            Должен иметь нулевые локальные координаты в объекте.")]
        public Transform meshRoot;

        // Время следующего изменения цели.
        private float nextRandomizeTime;

        public void Start()
        {
            agent.updatePosition = true;
            agent.updateRotation = true;
        }

        public void OnEnable()
        {
            // Не в Start() для возможной работы в пуле.
            transform.rotation = Quaternion.Euler(0, Random.value * 360, 0);
            RandomizeMovement();
        }

        public void FixedUpdate()
        {
            if (Time.fixedTime >= nextRandomizeTime)
            {
                RandomizeMovement();
            }

            // Так как поворот объекта задаётся агентом, поворачиваем корневой объект mesh'а
            meshRoot.rotation = GetRotationForDirection(transform.forward);

            // Так как возможны ситуации когда лошадь движется назад
            float speed = Vector3.Dot(agent.velocity, transform.forward);
            SetAnimatorSpeedParameter(speed);
        }

        private void RandomizeMovement()
        {
            float speed = RandomSpeed();
            if (speed < walkSpeed)
            {
                // Останавливаемся
                agent.ResetPath();
                return;
            }

            Vector2 offset = Random.insideUnitCircle * targetSelectionRadius;
            Vector3 movementTarget = transform.position;

            movementTarget.x += offset.x;
            movementTarget.z += offset.y;

            agent.speed = speed;
            bool result = agent.SetDestination(movementTarget);


            nextRandomizeTime = Time.fixedTime + Random.Range(minChangeInterval, maxChangeInterval);
        }

        private float RandomSpeed()
        {
            float f = Random.value;
            float speed;
            if (f < 0.33f)
            {
                speed = 0;
            }
            else if (f < 0.67f)
            {
                speed = walkSpeed;
            }
            else
            {
                speed = runSpeed;
            }

            return speed;
        }
    }
}