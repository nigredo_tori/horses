﻿using UnityEngine;
using System.Collections;
using System;

namespace Horse.Actor
{
    /// <summary>
    /// Управляет выносливостью лошади.
    /// 
    /// Выносливость уменьшается со скоростью, пропорциональной скорости движения лошади.
    /// </summary>
    [Serializable]
    public class HorseStaminaModel
    {

        private const float EPSILON = 0.01f;

        [Tooltip("Скорость восстановления энергии")]
        public float regenerationRate = 0.25f;

        [Tooltip("Скорость лошади, при которой энергия не меняется")]
        public float neutralSpeed = 5f;

        /// <summary>
        /// Энергия лошади в интервале [0, 1]. Если энергия на нуле - лошадь не может скакать галопом.
        /// </summary>
        public float Stamina { get; private set; }

        public bool CanRun
        {
            get
            {
                return Stamina > EPSILON;
            }
        }

        /// <summary>
        /// Сбросить данные - заполнить шкалу выносливости.
        /// </summary>
        public void Reset()
        {
            Stamina = 1;
        }

        /// <summary>
        /// Обновить состояние в зависимости от текущей скорости лошади и истекшего времени.
        /// </summary>
        /// <param name="deltaTime"></param>
        /// <param name="speed">Скорость лошади</param>
        public void Update(float deltaTime, float speed)
        {
            float staminaChangeRate = (neutralSpeed - speed) / neutralSpeed * regenerationRate;
            float deltaStamina = deltaTime * staminaChangeRate;

            Stamina = Mathf.Clamp01(Stamina + deltaStamina);
        }
    }
}