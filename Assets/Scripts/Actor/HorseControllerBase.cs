﻿using UnityEngine;
using System.Collections;

namespace Horse.Actor
{
    /// <summary>
    /// Базовый класс для контроллеров лошади.
    /// 
    /// Реализует управление анимацией, содержит общие свойства
    /// </summary>
    public class HorseControllerBase : MonoBehaviour
    {

        [Tooltip("Скорость движения рысью, м/с")]
        public float walkSpeed = 5;

        [Tooltip("Скорость движения галопом, м/с")]
        public float runSpeed = 10;

        [Tooltip("Ускорение лошади, м/с^2")]
        public float acceleration = 8;

        [Tooltip("Скорость поворота лошади, рад/с")]
        public float angularSpeed = 1;

        [Tooltip("Скорость движения назад, м/с")]
        public float backUpSpeed = 3;

        // идентификатор параметра аниматора.
        private int speedParameterId;

        private Animator animator;
        protected NavMeshAgent agent;

        public virtual void Awake()
        {
            animator = GetComponent<Animator>();
            agent = GetComponent<NavMeshAgent>();

            speedParameterId = Animator.StringToHash("Speed");

            agent.acceleration = acceleration;
            agent.angularSpeed = Mathf.Rad2Deg * angularSpeed;
            agent.speed = runSpeed;
        }

        protected void SetAnimatorSpeedParameter(float speed)
        {
            animator.SetFloat(speedParameterId, speed);
        }

        /// <summary>
        /// Возвращает вращение лошади, такое что проекции direction и локальной оси z на
        ///  глобальную плоскость xz сонаправлены, а сама локальная ось z параллельна поверхности NavMesh
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        protected Quaternion GetRotationForDirection(Vector3 direction)
        {
            // Позиция агента
            Vector3 basePos = transform.position - agent.baseOffset * Vector3.up;

            // Точка чуть впереди лошади
            Vector3 forwardPos = basePos + agent.radius * direction;

            NavMeshHit hit;
            NavMesh.SamplePosition(forwardPos, out hit, agent.radius, NavMesh.AllAreas);

            Vector3 delta = hit.position - basePos;

            Quaternion result = Quaternion.LookRotation(delta, Vector3.up);
            return result;
        }
    }
}