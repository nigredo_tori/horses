﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

namespace Horse.Actor
{
    /// <summary>
    /// Реализует управление лошадью при помощи клавиатуры/тачскрина.
    /// 
    /// Использует CrossPlatformInputManager для получения ввода.
    /// Используемые оси и кнопки:
    /// - Horizontal - поворот
    /// - Vertical - ускорение/торможение/движение назад
    /// - Run - галоп
    /// </summary>
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(NavMeshAgent))]
    public class HorseUserController : HorseControllerBase
    {

        [Tooltip("Минимальная разница между текущей скоростью и скоростью агента, при которой происходит коррекция.")]
        public float speedCorrectionTreshold = 0.5f;

        [Tooltip("Transform, управляющий положением отображаемой лошади")]
        public Transform horseMeshTransform;

        public HorseStaminaModel staminaModel;

        // Скорость лошади в данный момент. Отрицательная если лошадь движется назад.
        private float currentSpeed;

        // Поворот относительно оси Y в радианах
        private float currentRotationY;
        
        public override void Awake()
        {
            base.Awake();
            agent.speed = runSpeed;

            agent.updateRotation = false;
            agent.updatePosition = true;

            staminaModel.Reset();

            currentRotationY = transform.eulerAngles.y * Mathf.Deg2Rad;
        }
        
        void FixedUpdate()
        {
            // Пользовательский ввод
            float horizontalInput = CrossPlatformInputManager.GetAxis("Horizontal");
            float verticalInput = CrossPlatformInputManager.GetAxis("Vertical");
            bool runInput = CrossPlatformInputManager.GetButton("Run");

            UpdateCurrentSpeed(verticalInput, runInput);
            UpdateCurrentRotation(horizontalInput);

            SetAnimatorSpeedParameter(currentSpeed);

            // Вычисляем rotation

            // Направление "вперёд" на плоскости xz
            Vector3 forwardXZ = new Vector3(Mathf.Sin(currentRotationY), 0, Mathf.Cos(currentRotationY));
            
            transform.rotation = GetRotationForDirection(forwardXZ);

            agent.velocity = transform.forward * currentSpeed;

            // Обновляем модель выносливости
            staminaModel.Update(Time.fixedDeltaTime, Mathf.Abs(currentSpeed));
        }

        /// <summary>
        /// Вычисляет новое значение поворота вокруг оси Y
        /// </summary>
        /// <param name="horizontalInput"></param>
        private void UpdateCurrentRotation(float horizontalInput)
        {
            float deltaRotationDegrees = horizontalInput * angularSpeed * Time.fixedDeltaTime;
            currentRotationY += deltaRotationDegrees;
        }

        /// <summary>
        /// Вычисляет новое значение скорости.
        /// </summary>
        /// <param name="verticalInput"></param>
        /// <param name="runInput"></param>
        private void UpdateCurrentSpeed(float verticalInput, bool runInput)
        {
            float agentSpeed = Vector3.Dot(agent.velocity, transform.forward);

            if (Mathf.Abs(agentSpeed - currentSpeed) > speedCorrectionTreshold)
            {
                // Скорость агента по какой-то причине изменилась - синхронизируем.
                currentSpeed = agentSpeed;
            }


            // Максимальная скорость для текущего режима передвижения
            float modeMaxSpeed;
            if (verticalInput < 0)
            {
                modeMaxSpeed = backUpSpeed;
            }
            else if (runInput)
            {
                modeMaxSpeed = runSpeed;
            }
            else
            {
                modeMaxSpeed = walkSpeed;
            }

            float targetSpeed = verticalInput * modeMaxSpeed;

            // Если энергии недостаточно - устанавливаем верхний предел скорости.
            if (!staminaModel.CanRun)
            {
                targetSpeed = Mathf.Min(targetSpeed, staminaModel.neutralSpeed);
            }

            currentSpeed = Mathf.MoveTowards(currentSpeed, targetSpeed, acceleration * Time.fixedDeltaTime);
        }
    }

}