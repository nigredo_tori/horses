﻿using UnityEngine;
using System.Collections;

namespace Horse
{
    /// <summary>
    /// Позиционирование камеры
    /// </summary>
    public class CameraFollow : MonoBehaviour
    {

        public Transform target;

        [Tooltip("Точка в локальных координатах камеры, которая будет совпадать с позицией цели")]
        public Vector3 camTargetPosition;

        void Update()
        {
            transform.rotation = Quaternion.Euler(0, target.rotation.eulerAngles.y, 0);
            transform.position += target.position - transform.TransformPoint(camTargetPosition);
        }
    }
}